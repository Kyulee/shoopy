<center>
<img width=100 src="https://www.j2store.org/images/themeparrot/home_page/shopping-cart.png " /> 
<h1>Shoopy</h1>
</center>
Cette application est une petite application d'e-commerce dans le cadre d'un projet d'école en React Native.

<h2>Technologies utilisées</h2>

- Expo
- XCode
- React Native
- ReduxToolKit/ React thunk
- Insomnia
- Firebase

<h2>Liste des commandes de déploiement</h2>

- Installation

```
yarn install
```

- Lancement

```
expo start
```

<h2>Dependance en plus pour lancer le projet via XCode (seulement sur Mac)</h2>

[Doc d'installation XCode](https://docs.expo.io/workflow/ios-simulator/)

<h2>Diagramme de communication de l'application</h2>
<a  href="https://zupimages.net/viewer.php?id=21/21/m48k.png"><img width=300 src="https://zupimages.net/up/21/21/m48k.png" alt="" />
