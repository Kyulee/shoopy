import React, { useState, useEffect } from "react";
import {
  SafeAreaView,
  View,
  KeyboardAvoidingView,
  StyleSheet,
  TouchableWithoutFeedback,
  Button,
  Keyboard,
  ScrollView,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { Input, Avatar } from "react-native-elements";
import { pickImage } from "@services/utils/shared";
import { editProduct, getProducts } from "@store/slices/thunks/productThunks";

const EditProductScreen = ({ route, navigation }: any) => {
  const { idProduct } = route.params;
  const products = useSelector((state: any) => state.product.product.content);
  const dispatch: any = useDispatch();
  const product: any = products.find((p: any) => idProduct == p?.id);

  const [form, setFormValue] = useState({ ...product });

  useEffect(() => {
    setFormValue({ ...product });
  }, [idProduct]);

  const onSubmit = (e: any) => {
    dispatch(
      editProduct({
        product: {
          ...form,
        },
        id: idProduct,
      })
    ).then(() => dispatch(getProducts()));
    Keyboard.dismiss();
    navigation.navigate("UserOverviewProduct");
  };
  return (
    <SafeAreaView style={{ flex: 1, marginBottom: 15 }}>
      <KeyboardAvoidingView
        behavior="padding"
        style={{
          flex: 1,
        }}
      >
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <ScrollView>
            <View style={styles.avatarContainer}>
              <Avatar
                activeOpacity={0.7}
                containerStyle={styles.avatar}
                source={
                  form.photo && form.photo !== "" ? { uri: form.photo } : null
                }
                size="xlarge"
              >
                <Avatar.Accessory
                  size={50}
                  onPress={() =>
                    pickImage()
                      .then((res: any) => setFormValue({ ...form, photo: res }))
                      .catch((error) => console.error(error))
                  }
                />
              </Avatar>
            </View>
            <Input
              label="Name"
              value={form.name}
              onChangeText={(name) => setFormValue({ ...form, name })}
            />
            <Input
              label="Price"
              keyboardType="numeric"
              onChangeText={(price) => setFormValue({ ...form, price })}
              value={form.price.toString()}
            />
            <Input
              label="Description"
              value={form.description}
              multiline={true}
              onChangeText={(description) =>
                setFormValue({ ...form, description })
              }
              numberOfLines={10}
            />
            <View>
              <Button title="Submit changes" onPress={onSubmit} />
            </View>
          </ScrollView>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default EditProductScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  avatarContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  avatar: {
    width: "100%",
    height: 300,
    borderWidth: 1,
    borderColor: "white",
    marginBottom: 10,
  },
  inner: {
    padding: 24,
    flex: 1,
    justifyContent: "space-around",
  },
  header: {
    fontSize: 36,
    marginBottom: 48,
  },
  textInput: {
    height: 40,
    borderColor: "#000000",
    borderBottomWidth: 1,
    marginBottom: 36,
  },
  btnContainer: {
    backgroundColor: "white",
    marginTop: 12,
  },
});
