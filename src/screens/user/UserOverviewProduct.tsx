import React from "react";
import { SafeAreaView, ScrollView } from "react-native";
import ProductCard from "@components/ProductCard";
import { useSelector } from "react-redux";

const UserOverviewProduct = (props: any) => {
  const products = useSelector((state: any) => state.product.product);

  const { content } = products;

  return (
    <SafeAreaView style={{ flex: 1, marginBottom: 15 }}>
      <ScrollView>
        {content.map((product: any, index: any) => (
          <ProductCard {...props} key={index} product={product} isAdmin />
        ))}
      </ScrollView>
    </SafeAreaView>
  );
};
export default UserOverviewProduct;
