import React, { useState } from "react";
import {
  View,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Button,
  Keyboard,
  Platform,
  ScrollView,
} from "react-native";
import { Input, Avatar } from "react-native-elements";
import { useDispatch } from "react-redux";

import { pickImage } from "@services/utils/shared";
import AppStyle, { navigation } from "@styles/AppStyle";
import { addProduct, getProducts } from "@store/slices/thunks/productThunks";
import { useNavigation } from "@react-navigation/native";

const AddProductScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const initialState = {
    name: "",
    photo: "",
    price: "",
    description: "",
  };

  const [form, setFormValue] = useState(initialState);

  const onSubmit = (e: any) => {
    dispatch(
      addProduct({
        product: {
          ...form,
        },
      })
    ).then(() => {
      setFormValue(initialState);
      Keyboard.dismiss();
      dispatch(getProducts());
      navigation.navigate("UserOverviewProduct");
    });
  };

  return (
    <KeyboardAvoidingView
      enabled
      behavior={Platform.OS === "ios" ? "padding" : null}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <ScrollView>
          <View style={AppStyle.screens.addProduct.avatarContainer}>
            <Avatar
              activeOpacity={0.7}
              containerStyle={AppStyle.screens.addProduct.avatar}
              size="xlarge"
              source={
                form.photo && form.photo !== "" ? { uri: form.photo } : null
              }
            >
              <Avatar.Accessory
                size={50}
                onPress={() =>
                  pickImage()
                    .then((res: any) => {
                      setFormValue({ ...form, photo: res });
                    })
                    .catch((error) => console.error(error))
                }
              />
            </Avatar>
          </View>
          <Input
            label="Name"
            onChangeText={(name) => setFormValue({ ...form, name })}
          />
          <Input
            label="Price"
            keyboardType="numeric"
            onChangeText={(price) => setFormValue({ ...form, price })}
          />
          <Input
            label="Description"
            multiline={true}
            numberOfLines={10}
            onChangeText={(description) =>
              setFormValue({ ...form, description })
            }
          />
          <View>
            <Button title="Add new product" onPress={onSubmit} />
          </View>
        </ScrollView>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
};
export default AddProductScreen;
