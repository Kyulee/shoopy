import React, { useEffect } from "react";
import { SafeAreaView, View } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import OrderCard from "@components/OrderCard";
import { useSelector, useDispatch } from "react-redux";

import { getOrders } from "@store/slices/thunks/orderThunks";

const OrderScreen = () => {
  const orders = useSelector((state: any) => state.product.order);
  const dispatch = useDispatch();

  const { content } = orders;

  useEffect(() => {
    dispatch(getOrders());
  }, [dispatch]);

  return (
    <SafeAreaView style={{ flex: 1, marginBottom: 15 }}>
      <View>
        <ScrollView>
          {content.map((order: any) => (
            <OrderCard order={order} key={order.id} />
          ))}
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

export default OrderScreen;
