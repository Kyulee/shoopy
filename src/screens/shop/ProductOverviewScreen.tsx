import React, { useEffect } from "react";
import { SafeAreaView } from "react-native";
import { ScrollView } from "react-native-gesture-handler";

import Loader from "@components/Loader";
import ProductCard from "@components/ProductCard";
import { useDispatch, useSelector } from "react-redux";
import { getProducts } from "@store/slices/thunks/productThunks";
import { AsyncStatus } from "@services/utils/shared";

const ProductOverviewScreen = () => {
  const { readStatus, content: products } = useSelector(
    (state: any) => state.product.product
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getProducts());
  }, [dispatch]);
  if (readStatus === AsyncStatus.LOADING) return <Loader />;
  return (
    <SafeAreaView style={{ flex: 1, marginBottom: 15 }}>
      <ScrollView>
        {products.map((product: any, index: any) => (
          <ProductCard key={index} product={product} />
        ))}
      </ScrollView>
    </SafeAreaView>
  );
};

export default ProductOverviewScreen;
