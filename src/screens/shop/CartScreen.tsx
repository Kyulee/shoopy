import React, { useEffect } from "react";
import {
  View,
  Text,
  ScrollView,
  SafeAreaView,
  ActivityIndicator,
} from "react-native";
import { Button } from "react-native-elements";
import { useNavigation } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";

import CartItem from "@components/CartItem";
import { updateTotal, updateTotalQuantity } from "@store/slices/cartSlice";
import AppStyle from "@styles/AppStyle";
import { resetCart } from "@store/slices/cartSlice";
import { AsyncStatus } from "services/utils/shared";
import { addOrder } from "store/slices/thunks/orderThunks";

const CartScreen = () => {
  const navigation = useNavigation();
  const dispatch: any = useDispatch();
  const cart = useSelector((state: any) => state?.product?.cart);
  const status = useSelector(
    (state: any) => state?.product?.order?.writeStatus
  );

  const { content, totalAmount } = cart;

  useEffect(() => {
    dispatch(updateTotal());
    dispatch(updateTotalQuantity());
  }, [content]);

  return (
    <SafeAreaView style={AppStyle.screens.cart.screenCart}>
      <ScrollView>
        {content && content.length > 0 ? (
          content.map((product: any, index: number) => (
            <CartItem product={product} key={index}></CartItem>
          ))
        ) : (
          <View>
            <Text style={AppStyle.screens.cart.noProductText}>
              No products in the cart
            </Text>
            <Button
              title="Browse catalog"
              titleStyle={AppStyle.screens.cart.buttonBrowseTitle}
              buttonStyle={AppStyle.screens.cart.buttonBrowse}
              onPress={() => navigation.navigate("Shop")}
            />
          </View>
        )}
      </ScrollView>
      <View style={AppStyle.screens.cart.card}>
        <Text style={AppStyle.screens.cart.total}>
          Total : {totalAmount.toFixed(2)} €
        </Text>
        {content &&
          content.length > 0 &&
          (status === AsyncStatus.LOADING ? (
            <ActivityIndicator size="small" color="deeppink" />
          ) : (
            <Button
              title="Order Now"
              type="clear"
              titleStyle={AppStyle.screens.cart.buttonOrderTitle}
              onPress={() => {
                dispatch(
                  addOrder({
                    newOrder: {
                      items: [...content],
                      totalOrder: totalAmount,
                    },
                  })
                ).then(() => {
                  dispatch(resetCart());
                  navigation.navigate("Order");
                });
              }}
            />
          ))}
      </View>
    </SafeAreaView>
  );
};

export default CartScreen;
