import React from "react";
import { View, Text, Image, StyleProp, ViewStyle } from "react-native";
import { Button } from "react-native-elements";
import { useDispatch, useSelector } from "react-redux";

import { addProduct, updateTotalQuantity } from "@store/slices/cartSlice";
import AppStyle from "@styles/AppStyle";

const ProductDetailsScreen = ({ route, navigation }: any) => {
  const { idProduct } = route.params;

  const products = useSelector((state: any) => state.product.product.content);
  const dispatch = useDispatch();
  const product: any = products.find((p: any) => idProduct == p?.id);

  return (
    <View style={AppStyle.screens.productDetails.cart as StyleProp<ViewStyle>}>
      <Image
        style={AppStyle.screens.productDetails.photo}
        source={{
          uri: product.photo,
        }}
      />
      <Text style={AppStyle.screens.productDetails.title}>{product.name}</Text>
      <Text style={AppStyle.screens.productDetails.price}>
        {product.price}€
      </Text>
      <Text style={AppStyle.screens.productDetails.description}>
        {product.description}
      </Text>
      <Button
        style={AppStyle.screens.productDetails.button}
        title="Add to cart"
        type="outline"
        onPress={() =>
          dispatch(addProduct({ product })) && dispatch(updateTotalQuantity())
        }
      />
    </View>
  );
};

export default ProductDetailsScreen;
