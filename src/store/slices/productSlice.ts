import { createSlice } from "@reduxjs/toolkit";
import { AsyncStatus } from "@services/utils/shared";
import {
  getProducts,
  addProduct,
  deleteProduct,
  editProduct,
} from "./thunks/productThunks";
import { Alert } from "react-native";

const productSlice = createSlice({
  name: "product",
  initialState: {
    content: [] as any,
    readStatus: AsyncStatus.NONE,
    writeStatus: AsyncStatus.NONE,
  },
  reducers: {
    initProduct: (state, { payload }) => ({
      ...state,
      content: payload.content,
    }),
    addNewProduct: (state, { payload }): any => {
      const content: any = [...state.content] || [];
      const { product } = payload;
      content.push({ ...product, id: `p${Math.floor(Math.random() * 1000)}` });

      return { ...state, content };
    },
    editProduct: (state, { payload }): any => {
      const content: any = [...state.content] || [];
      const { product } = payload;
      const index = content.findIndex((p: any) => p.id === product.id);
      if (index !== -1) {
        content.splice(index, 1);
        content.push(product);
      }
      return { ...state, content };
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getProducts.fulfilled, (state, { payload }) => {
      state.content = payload;
      state.readStatus = AsyncStatus.SUCCESS;
    });
    builder.addCase(getProducts.pending, (state, action) => {
      state.readStatus = AsyncStatus.LOADING;
    });
    builder.addCase(getProducts.rejected, (state, action) => {
      state.readStatus = AsyncStatus.ERROR;
      Alert.alert(
        "Erreur",
        "Problème est survenu lors de la récupération des produits, veuillez reessayez."
      );
    });
    builder.addCase(addProduct.fulfilled, (state, { payload }: any) => {
      state.writeStatus = AsyncStatus.SUCCESS;
    });
    builder.addCase(addProduct.pending, (state, { payload }) => {
      state.writeStatus = AsyncStatus.LOADING;
    });

    builder.addCase(addProduct.rejected, (state, action) => {
      state.writeStatus = AsyncStatus.ERROR;
      Alert.alert(
        "Erreur",
        "Problème est survenu lors de l'enregistrement du produit, veuillez reessayez."
      );
    });
    builder.addCase(deleteProduct.fulfilled, (state, { payload }: any) => {
      state.writeStatus = AsyncStatus.SUCCESS;
    });
    builder.addCase(deleteProduct.pending, (state, { payload }) => {
      state.writeStatus = AsyncStatus.LOADING;
    });

    builder.addCase(deleteProduct.rejected, (state, action) => {
      state.writeStatus = AsyncStatus.ERROR;
      Alert.alert(
        "Erreur",
        "Problème est survenu lors de la suppresion du produit, veuillez reessayez."
      );
    });
    builder.addCase(editProduct.fulfilled, (state, { payload }: any) => {
      state.writeStatus = AsyncStatus.SUCCESS;
    });
    builder.addCase(editProduct.pending, (state, { payload }) => {
      state.writeStatus = AsyncStatus.LOADING;
    });
    builder.addCase(editProduct.rejected, (state, action) => {
      state.writeStatus = AsyncStatus.ERROR;
      Alert.alert(
        "Erreur",
        "Problème est survenu lors de la édition du produit, veuillez reessayez."
      );
    });
  },
});

export const { initProduct } = productSlice.actions;
export default productSlice;
