import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  content: [],
  totalAmount: 0,
  totalQty: 0,
};
const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    initCart: (state, { payload }) => ({ ...state, content: payload.content }),
    resetCart: (state) => initialState,
    updateTotal: (state): any => ({
      ...state,
      totalAmount: state.content.reduce(
        (acc, p: any) => acc + p.price * p.qty,
        0
      ),
    }),
    addProduct: (state, { payload }): any => {
      const content: any = [...state.content] || [];
      const { product } = payload;
      const index = content.findIndex((p: any) => p.id === product.id);
      let productToAdd = { ...product, qty: 1 };
      if (index != -1) {
        productToAdd = { ...content[index], qty: content[index].qty + 1 };
        content.splice(index, 1);
      }
      content.push(productToAdd);
      return { ...state, content };
    },
    increaseQuantity: (state, { payload }): any => {
      const content: any = [...state.content] || [];
      const index = content.findIndex((p: any) => p.id === payload.idProduct);
      if (index != -1) {
        let productToIncrease = {
          ...content[index],
          qty: content[index].qty + 1,
        };
        content.splice(index, 1);
        content.splice(index, 0, productToIncrease);
      }
      return { ...state, content };
    },
    decreaseQuantity: (state, { payload }: any) => {
      const content: any = [...state.content] || [];
      const index = content.findIndex((p: any) => p.id === payload.idProduct);
      if (index != -1) {
        let productToDelete = {
          ...content[index],
          qty: content[index].qty - 1,
        };
        content.splice(index, 1);
        if (productToDelete.qty !== 0)
          content.splice(index, 0, productToDelete);
      }
      return { ...state, content };
    },
    deleteProduct: (state, { payload }): any => {
      const content: any = [...state.content] || [];
      const index = content.findIndex((p: any) => p.id === payload.idProduct);
      if (index != -1) {
        content.splice(index, 1);
      }
      return { ...state, content };
    },
    updateTotalQuantity: (state) => ({
      ...state,
      totalQty: state.content.reduce((acc, p: any) => acc + p.qty, 0),
    }),
  },
});

export const {
  initCart,
  addProduct,
  increaseQuantity,
  decreaseQuantity,
  deleteProduct,
  updateTotal,
  updateTotalQuantity,
  resetCart,
} = cartSlice.actions;
export default cartSlice;
