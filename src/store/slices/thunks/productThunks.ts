import axios from "axios";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { getDatabase } from "@services/config/firebase";

const productUrl = `https://shoopy-app-default-rtdb.europe-west1.firebasedatabase.app/products.json`;
//https://shoopy-app-default-rtdb.europe-west1.firebasedatabase.app/products/id.json
export const getProducts = createAsyncThunk(
  "products/getProducts",
  async () => {
    const db = getDatabase();
    const products = await db.ref("products/").get();
    return Object.entries(products.val()).map(([key, value]) => value);
  }
);

export const addProduct: any = createAsyncThunk(
  "products/addProduct",
  async ({ product }: any) => {
    console.log(product);
    const db = getDatabase();
    const newKey = db.ref("products").push().key;
    const payload = {
      [`p${newKey}`]: { ...product, id: `p${newKey}` },
    };
    const addProduct = await db.ref("products").update(payload);
    return addProduct;
  }
);

export const deleteProduct = createAsyncThunk(
  "products/deleteProduct",
  async ({ idProduct }: any) => {
    const db = getDatabase();
    const deleteProduct = await db.ref(`products/${idProduct}`).remove();
    return deleteProduct;
  }
);

export const editProduct = createAsyncThunk(
  "products/editProduct",
  async ({ id, product }: any) => {
    const db = getDatabase();
    const payload = {
      [id]: { ...product, id },
    };
    const editProduct = await db.ref("products").update(payload);
    return editProduct;
  }
);
