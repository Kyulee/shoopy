import axios from "axios";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { getDatabase } from "@services/config/firebase";

const orderUrl = `https://shoopy-app-default-rtdb.europe-west1.firebasedatabase.app/orders.json`;

export const getOrders = createAsyncThunk("orders/getOrders", async () => {
  const db = getDatabase();
  const products = await db.ref("orders").get();
  return Object.entries(products.val()).map(([key, value]) => value);
});

export const addOrder = createAsyncThunk(
  "orders/addOrder",
  async ({ newOrder, navigation }: any) => {
    const db = getDatabase();
    const newKey = db.ref("orders").push().key;
    const date = new Date().getTime();

    const payload = {
      [`o${newKey}`]: { ...newOrder, id: `o${newKey}`, date },
    };
    const addOrder = await db.ref("orders").update(payload);
    return { addOrder, navigation };
  }
);
