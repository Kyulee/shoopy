import { AsyncStatus } from "./../../services/utils/shared";
import { createSlice } from "@reduxjs/toolkit";
import { Alert } from "react-native";
import { getOrders, addOrder } from "./thunks/orderThunks";
import {
  genericErrorCase,
  genericLoadingCase,
} from "@services/utils/redux.utils";

const extraReducers = [getOrders];

const orderSlice = createSlice({
  name: "order",
  initialState: {
    content: [] as any,
    readStatus: AsyncStatus.NONE,
    writeStatus: AsyncStatus.NONE,
  },
  reducers: {
    initOrder: (state, { payload }) => ({ ...state, content: payload.content }),

    // addOrder: (state, { payload }) => {
    //   const content: any = [...state.content] || [];
    //   const { items, totalOrder } = payload;
    // const maxId = content.reduce(
    //   (acc: any, order: any) => (order.id > acc ? order.id : acc),
    //   0
    // );
    //   const id = maxId + 1;
    //   content.push({ items, date, id, totalOrder });
    //   return { ...state, content };
    // },
  },
  extraReducers: (builder) => {
    for (const red of extraReducers) {
      builder.addCase(red.fulfilled, (state, { payload }) => {
        state.readStatus = AsyncStatus.SUCCESS;
        state.content = Object.entries(payload).map(([key, value]) => value);
      });
      builder.addCase(red.rejected, genericErrorCase);
      builder.addCase(red.pending, genericLoadingCase);
    }

    builder.addCase(addOrder.fulfilled, (state, { payload }) => {
      state.writeStatus = AsyncStatus.SUCCESS;
    });
    builder.addCase(addOrder.pending, (state, action) => {
      state.writeStatus = AsyncStatus.LOADING;
    });
    builder.addCase(addOrder.rejected, (state, action) => {
      state.writeStatus = AsyncStatus.ERROR;
      Alert.alert(
        "Erreur",
        "Problème est survenu lors de l'enregistrement de la commande, veuillez reessayez."
      );
    });
  },
});

export const { initOrder } = orderSlice.actions;
export default orderSlice;
