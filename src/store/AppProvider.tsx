import React from "react";
import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { Provider } from "react-redux";

import cartSlice from "./slices/cartSlice";
import orderSlice from "./slices/orderSlice";
import productSlice from "./slices/productSlice";

const productReducers = combineReducers({
  cart: cartSlice.reducer,
  order: orderSlice.reducer,
  product: productSlice.reducer,
});
const store = configureStore({
  reducer: {
    product: productReducers,
  },
});

export const ReactProvider = ({ children }: any) => (
  <Provider store={store}>{children}</Provider>
);
