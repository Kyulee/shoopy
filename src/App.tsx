import React, { useEffect, useState } from "react";
import "react-native-gesture-handler";
import { useColorScheme } from "react-native";
import * as Font from "expo-font";

import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
} from "@react-navigation/native";
import AppNavigator from "./components/Navigation";

import { ReactProvider } from "./store/AppProvider";
import { initFirebase } from "services/config/firebase";

const lightTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: "#C2185B",
    secondary: "#FFC1077",
  },
};

const darkTheme = {
  ...DarkTheme,
  colors: {
    ...DarkTheme.colors,
    primary: "#C2185B",
    secondary: "#FFC1077",
    background: "#272727",
  },
};

const loadFonts = async () => {
  await Font.loadAsync({
    "OpenSans-Bold": require("./assets/fonts/OpenSans-Bold.ttf"),
    "OpenSans-Regular": {
      uri: require("./assets/fonts/OpenSans-Bold.ttf"),
      display: Font.FontDisplay.FALLBACK,
    },
  });
};

const App = () => {
  initFirebase();
  const [isFontLoaded, setIsLoaded] = useState(false);

  const scheme: any = useColorScheme();
  const isDark = scheme === "light";

  useEffect(() => {
    loadFonts().then(() => setIsLoaded(!isFontLoaded));
  }, []);

  if (!isFontLoaded) {
    return null;
  }

  return (
    <NavigationContainer
      // theme={scheme == "light" ? lightTheme : darkTheme}
      theme={lightTheme}
    >
      <AppNavigator />
    </NavigationContainer>
  );
};

export default () => (
  <ReactProvider>
    <App />
  </ReactProvider>
);
