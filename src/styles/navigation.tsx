import { StyleSheet } from "react-native";

const backgroundColor = "#C2185B";

const headers = StyleSheet.create({
  container: {
    backgroundColor,
  },
  title: {
    color: "#ffffff",
    fontFamily: "OpenSans-Bold",
  },
});
const navbar = StyleSheet.create({
  menuButton: {
    backgroundColor,
  },
  cartButton: {
    backgroundColor,
  },
  backButton: {
    backgroundColor,
  },
});
const drawerContainer = StyleSheet.create({
  self: {
    flex: 1,
    justifyContent: "flex-start",
    backgroundColor: "rgba(26,26,29,1)",
    width: "100%",
    paddingTop: "10%",
  },
  navigationContainer: {
    marginBottom: 30,
  },
  adminTitle: {
    paddingLeft: 5,
    fontSize: 28,
    color: "#fff",
  },
  divider: {
    alignSelf: "center",
    backgroundColor: "rgba(50,50,50,1)",
    width: "70%",
    marginVertical: 20,
  },
});

const drawerItem = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginVertical: 0,
    width: "100%",
  },

  containerActive: {
    backgroundColor: "rgba(203,37,70,0.2)",
  },

  textLabel: {
    fontFamily: "OpenSans-Bold",
    fontSize: 20,
    fontWeight: "100",
    color: "#FAFAFC",
    marginHorizontal: 20,
  },

  icon: {
    width: 24,
    height: 24,
    marginHorizontal: 0,
    marginRight: 20,
  },

  leftBar: {
    position: "absolute",
    left: 0,
    width: 4,
    height: "160%",
    backgroundColor: "red",
  },
});

const drawerProfile = StyleSheet.create({
  container: {
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 30,
  },
  avatar: {
    height: 80,
    width: 80,
    borderWidth: 1,
    borderColor: "white",
    marginBottom: 10,
  },
  nameText: {
    fontFamily: "OpenSans-Bold",
    color: "white",
    fontSize: 18,
    marginBottom: 10,
  },
});

export default {
  headers,
  navbar,
  drawer: {
    container: drawerContainer,
    item: drawerItem,
    profile: drawerProfile,
  },
};
