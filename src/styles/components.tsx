import { StyleSheet } from "react-native";

const cart = StyleSheet.create({
  cartItem: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 15,
  },
  quantityHandlingView: {
    marginLeft: "auto",
    marginRight: "auto",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "white",
  },
  productName: {
    paddingLeft: 15,
    fontSize: 14,
    width: "55%",
  },
  quantityText: {
    fontSize: 14,
    paddingLeft: 10,
    paddingRight: 10,
  },
  trashButton: {
    backgroundColor: "transparent",
  },
  increaseButton: {
    borderWidth: 1,
    backgroundColor: "white",
  },
  decreaseButton: {
    borderWidth: 1,
    backgroundColor: "white",
  },
  price: {
    marginLeft: 10,
    paddingLeft: 10,
    fontSize: 12,
    width: "20%",
  },
});
const order = StyleSheet.create({
  photo: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    width: "100%",
    height: 200,
  },
  title: {
    margin: 10,
    textAlign: "center",
    fontSize: 20,
    fontFamily: "OpenSans-Bold",
  },
  price: {
    textAlign: "center",
    fontSize: 20,
    color: "grey",
  },
  card: {
    borderRadius: 5,
    paddingTop: 0,
    paddingLeft: 0,
    paddingRight: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    fontFamily: "OpenSans-Regular",

    elevation: 8,
  },
  button: {
    marginTop: 10,
    flexDirection: "row",
    justifyContent: "center",
  },
  orderDetail: {
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20,
    flexDirection: "row",
    justifyContent: "space-between",
  },
});

const card = StyleSheet.create({
  photo: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    width: "100%",
    height: 200,
  },
  title: {
    margin: 10,
    textAlign: "center",
    fontSize: 20,
    fontFamily: "OpenSans-Bold",
  },
  price: {
    textAlign: "center",
    fontSize: 20,
    color: "grey",
  },
  self: {
    borderRadius: 5,
    paddingTop: 0,
    paddingLeft: 0,
    paddingRight: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    fontFamily: "OpenSans-Regular",

    elevation: 8,
  },
  button: {
    marginTop: 10,
    flexDirection: "row",
    justifyContent: "space-around",
  },
});

export default {
  cart,
  order,
  card,
};
