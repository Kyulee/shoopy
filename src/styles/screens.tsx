import { StyleSheet } from "react-native";

const addProduct = StyleSheet.create({
  container: {
    flex: 1,
  },
  avatarContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  avatar: {
    width: "100%",
    height: 300,
    borderWidth: 1,
    borderColor: "white",
    marginBottom: 10,
  },
  inner: {
    padding: 24,
    flex: 1,
    justifyContent: "space-around",
  },
  header: {
    fontSize: 36,
    marginBottom: 48,
  },
  textInput: {
    height: 40,
    borderColor: "#000000",
    borderBottomWidth: 1,
    marginBottom: 36,
  },
  btnContainer: {
    backgroundColor: "white",
    marginTop: 12,
  },
});
const productDetails = StyleSheet.create({
  cart: {
    fontFamily: "OpenSans-Regular",
  },
  photo: {
    width: "100%",
    height: 300,
  },
  title: {
    margin: 10,
    textAlign: "center",
    fontSize: 20,
    fontFamily: "OpenSans-Bold",
  },
  price: {
    textAlign: "center",
    fontSize: 20,
    color: "grey",
  },
  button: {
    justifyContent: "flex-end",
  },
  description: {
    padding: 10,
    fontSize: 20,
    height: "45%",
  },
});
const cart = StyleSheet.create({
  screenCart: {
    flex: 1,
    fontFamily: "OpenSans-Regular",
    flexDirection: "column",
    justifyContent: "space-between",
    marginBottom: 25,
  },
  card: {
    marginTop: 10,
    marginLeft: 5,
    marginRight: 5,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
    borderWidth: 1,
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  total: {
    fontWeight: "bold",
    fontSize: 20,
  },
  noProductText: {
    textAlign: "center",
    fontSize: 25,
  },
  buttonOrderTitle: {
    color: "#FFC300",
    fontFamily: "OpenSans-Bold",
  },
  buttonBrowse: {
    alignSelf: "center",
    width: "25%",
  },
  buttonBrowseTitle: {
    color: "white",
    fontFamily: "OpenSans-Bold",
  },
});
export default { addProduct, productDetails, cart };
