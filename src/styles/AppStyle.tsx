import { StyleSheet } from "react-native";
import navigation from "./navigation";
import components from "./components";
import screens from "./screens";

const global = StyleSheet.create({
  app: {
    zIndex: 0,
  },
});

export { global, navigation, components, screens };

export default {
  global,
  navigation,
  components,
  screens,
};
