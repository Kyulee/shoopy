import React from "react";
import { Text, View } from "react-native";
import { Button } from "react-native-elements";
import { useDispatch } from "react-redux";
import { Ionicons } from "@expo/vector-icons";

import {
  deleteProduct,
  increaseQuantity,
  decreaseQuantity,
} from "@store/slices/cartSlice";
import AppStyle from "@styles/AppStyle";

const CartItem = ({ product }: any) => {
  const dispatch = useDispatch();

  return (
    <View style={AppStyle.components.cart.cartItem}>
      <Text style={AppStyle.components.cart.productName}>{product.name}</Text>
      <ProductQuantityHandling product={product} />
      <Text style={AppStyle.components.cart.price}>x {product.price} €</Text>

      <Button
        icon={<Ionicons name="md-trash" size={14} color="red" />}
        onPress={() => {
          dispatch(deleteProduct({ idProduct: product.id }));
        }}
        buttonStyle={AppStyle.components.cart.trashButton}
      />
    </View>
  );
};
const ProductQuantityHandling = ({ product }: any) => {
  const dispatch = useDispatch();
  return (
    <View style={AppStyle.components.cart.quantityHandlingView}>
      <Button
        icon={<Ionicons name="md-remove" size={14} color="grey" />}
        onPress={() => {
          dispatch(decreaseQuantity({ idProduct: product.id }));
        }}
        buttonStyle={AppStyle.components.cart.decreaseButton}
      />

      <Text style={AppStyle.components.cart.quantityText}>{product.qty}</Text>
      <Button
        icon={<Ionicons name="md-add" size={14} color="grey" />}
        onPress={() => {
          increaseQuantity;
          dispatch(increaseQuantity({ idProduct: product.id }));
        }}
        buttonStyle={AppStyle.components.cart.increaseButton}
      />
    </View>
  );
};

export default CartItem;
