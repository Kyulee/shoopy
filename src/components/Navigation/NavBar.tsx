import React from "react";
import { Ionicons } from "@expo/vector-icons";
import { Button, Badge } from "react-native-elements";
import { useNavigation } from "@react-navigation/native";
import { useIsDrawerOpen } from "@react-navigation/drawer";
import { useSelector } from "react-redux";
import AppStyle from "@styles/AppStyle";

export const MenuBurger = () => {
  const navigation: any = useNavigation();
  const isDrawerOpen = useIsDrawerOpen();
  const toggleDrawer = () => {
    isDrawerOpen ? navigation.closeDrawer() : navigation.openDrawer();
  };
  return (
    <Button
      onPress={toggleDrawer}
      icon={<Ionicons name="md-menu" size={32} color="white" />}
      buttonStyle={AppStyle.navigation.navbar.menuButton}
    />
  );
};

export const CartButton = () => {
  const navigation = useNavigation();
  const qty = useSelector((state: any) => state?.product.cart.totalQty);
  return (
    <Button
      onPress={() => {
        navigation.navigate("Cart");
      }}
      icon={
        <Ionicons name="md-cart" size={32} color="white">
          <Badge value={qty} />
        </Ionicons>
      }
      buttonStyle={AppStyle.navigation.navbar.cartButton}
    />
  );
};

export const AddNewProductButton = () => {
  const navigation = useNavigation();
  return (
    <Button
      onPress={() => {
        navigation.navigate("AddProduct");
      }}
      icon={<Ionicons name="add-circle-outline" size={30} color="white" />}
      buttonStyle={AppStyle.navigation.navbar.cartButton}
    />
  );
};

export const CancelButton = () => {
  const navigation = useNavigation();
  return (
    <Button
      onPress={() => navigation.goBack()}
      icon={<Ionicons name="close-outline" size={32} color="white" />}
      buttonStyle={AppStyle.navigation.navbar.cartButton}
    />
  );
};

export const ThemeButton = () => {
  return (
    <Button
      onPress={() => {}}
      icon={<Ionicons name="md-sunny" size={32} color="white" />}
      buttonStyle={AppStyle.navigation.navbar.cartButton}
    />
  );
};
export const BackButton = () => {
  const navigation = useNavigation();

  return (
    <Button
      onPress={() => navigation.goBack()}
      icon={<Ionicons name="md-arrow-back" size={32} color="white" />}
      buttonStyle={AppStyle.navigation.navbar.cartButton}
    />
  );
};
