import React from "react";
import {
  createStackNavigator,
  StackNavigationOptions,
} from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";

import {
  ProductOverviewScreen,
  ProductDetailsScreen,
  CartScreen,
  OrderScreen,
  AddProductScreen,
  EditProductScreen,
  UserOverviewProduct,
} from "@screens/index";
import { navigation } from "@styles/AppStyle";

import {
  BackButton,
  CartButton,
  MenuBurger,
  AddNewProductButton,
  CancelButton,
} from "./NavBar";
import DrawerNavigationContainer from "./Drawer";

export const defaultHeadersOptions: any = {
  headerLeft: () => <MenuBurger />,
  headerRight: () => <CartButton />,
  title: "Shoopy",
  headerStyle: navigation.headers.container,
  headerTitleStyle: navigation.headers.title,
};

export const ProductDetailsHeadersOptions: StackNavigationOptions = {
  headerLeft: () => <BackButton />,
  headerRight: () => <CartButton />,
  title: "Shoopy",
  headerStyle: navigation.headers.container,
  headerTitleStyle: navigation.headers.title,
  headerShown: true,
};

export const CartHeadersOptions: any = {
  headerLeft: () => <BackButton />,
  title: "Cart",
  headerStyle: navigation.headers.container,
  headerTitleStyle: navigation.headers.title,
  headerShown: true,
};

export const OrderHeadersOptions: any = {
  headerLeft: () => <BackButton />,
  title: "Your order",
  headerStyle: navigation.headers.container,
  headerTitleStyle: navigation.headers.title,
  headerShown: true,
};

export const AddProductHeadersOptions: any = {
  headerLeft: () => <CancelButton />,
  title: "Add a new product",
  headerStyle: navigation.headers.container,
  headerTitleStyle: navigation.headers.title,
  headerShown: true,
};

export const EditProductHeadersOptions: any = {
  headerLeft: () => <CancelButton />,
  title: "Edit a product",
  headerStyle: navigation.headers.container,
  headerTitleStyle: navigation.headers.title,
  headerShown: true,
};

export const UserOverviewProductHeadersOptions: any = {
  headerLeft: () => <MenuBurger />,
  headerRight: () => <AddNewProductButton />,
  title: "All your product",
  headerStyle: navigation.headers.container,
  headerTitleStyle: navigation.headers.title,
  headerShown: true,
};

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const StackNav = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Shop"
        component={ProductOverviewScreen}
        options={defaultHeadersOptions}
      />
      <Stack.Screen
        name="Product"
        component={ProductDetailsScreen}
        options={ProductDetailsHeadersOptions}
      />
      <Stack.Screen
        name="Cart"
        component={CartScreen}
        options={CartHeadersOptions}
      />
      <Stack.Screen
        name="Order"
        component={OrderScreen}
        options={OrderHeadersOptions}
      />
      <Stack.Screen
        name="AddProduct"
        component={AddProductScreen}
        options={AddProductHeadersOptions}
      />
      <Stack.Screen
        name="EditProduct"
        component={EditProductScreen}
        options={EditProductHeadersOptions}
      />
      <Stack.Screen
        name="UserOverviewProduct"
        component={UserOverviewProduct}
        options={UserOverviewProductHeadersOptions}
      />
    </Stack.Navigator>
  );
};

const AppNavigator = () => {
  return (
    <Drawer.Navigator drawerContent={DrawerNavigationContainer}>
      <Drawer.Screen
        name="Shop"
        component={StackNav}
        options={defaultHeadersOptions}
      />
      <Drawer.Screen
        name="Cart"
        component={CartScreen}
        options={CartHeadersOptions}
      />
      <Drawer.Screen
        name="Order"
        component={OrderScreen}
        options={OrderHeadersOptions}
      />
      <Drawer.Screen
        name="AddProduct"
        component={AddProductScreen}
        options={AddProductHeadersOptions}
      />
      <Drawer.Screen
        name="EditProduct"
        component={EditProductScreen}
        options={EditProductHeadersOptions}
      />
      <Drawer.Screen
        name="UserOverviewProduct"
        component={UserOverviewProduct}
        options={UserOverviewProductHeadersOptions}
      />
    </Drawer.Navigator>
  );
};
export default AppNavigator;
