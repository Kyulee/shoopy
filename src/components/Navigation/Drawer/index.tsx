/* eslint-disable import/no-extraneous-dependencies, import/no-unresolved, import/extensions */

import React from "react";
import { View, Text } from "react-native";
import { Divider } from "react-native-elements";

import DrawerItem from "./DrawerItem";
import DrawerProfileContainer from "./DrawerProfile";
import AppStyle from "@styles/AppStyle";

const profileInfo = {
  firstName: "Yuli",
  lastName: "Shoopy",
  imgSource: require("./../../../assets/images/yuli.jpeg"),
};

interface IDrawerItem {
  title: string;
  iconName: string;
  iconColor: string;
  screenName: string;
}

const drawersItems: Array<IDrawerItem> = [
  {
    title: "Catalog",
    iconName: "md-book",
    iconColor: "pink",
    screenName: "Shop",
  },
  {
    title: "Shopping Cart",
    iconName: "md-cart",
    iconColor: "pink",
    screenName: "Cart",
  },
  {
    title: "Your Orders",
    iconName: "list",
    iconColor: "pink",
    screenName: "Order",
  },
];

const drawersAdmin: Array<IDrawerItem> = [
  {
    title: "Add a new Product",
    iconName: "add-circle-outline",
    iconColor: "pink",
    screenName: "AddProduct",
  },
  {
    title: "Your Product",
    iconName: "folder",
    iconColor: "pink",
    screenName: "UserOverviewProduct",
  },
];

const generateDrawerItems = (route: any, index: any, navigation: any) => {
  const navigationLabel = route.title;
  const navigationIconName = route.iconName;
  const iconColor = route.iconColor;

  return (
    <React.Fragment key={route.screenName}>
      <DrawerItem
        label={navigationLabel}
        iconName={navigationIconName}
        iconColor={iconColor}
        onPress={() => {
          navigation.navigate(route.screenName);
        }}
      />
    </React.Fragment>
  );
};
const DrawerNavigationContainer = ({ navigation }: any) => {
  const { imgSource, ...rest } = profileInfo; // A remplacer par un useSelector pour récupéré les info user avatar etc
  const isAdmin = true; // A remplacer par un useSelector pour récupéré la state Admin de l'utilisateur;
  return (
    <View style={AppStyle.navigation.drawer.container.self}>
      <DrawerProfileContainer source={imgSource} {...rest} />
      <View style={AppStyle.navigation.drawer.container.self}>
        {drawersItems.map((route: any, index: any) =>
          generateDrawerItems(route, index, navigation)
        )}
      </View>
      {isAdmin ? (
        <View style={AppStyle.navigation.drawer.container.self}>
          <Divider style={AppStyle.navigation.drawer.container.divider} />
          <Text style={AppStyle.navigation.drawer.container.adminTitle}>
            Administration
          </Text>
          {drawersAdmin.map((route: any, index: any) =>
            generateDrawerItems(route, index, navigation)
          )}
        </View>
      ) : (
        <></>
      )}
    </View>
  );
};

export default DrawerNavigationContainer;
