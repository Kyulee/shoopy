import React from "react";
import { TouchableOpacity, View, Text } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import AppStyle from "@styles/AppStyle";

const DrawerItem = (props: any) => {
  const { label, iconName, iconColor, isActive } = props;
  const containerStyles = isActive
    ? [
        AppStyle.navigation.drawer.item.container,
        AppStyle.navigation.drawer.item.containerActive,
      ]
    : [AppStyle.navigation.drawer.item.container];
  return (
    <TouchableOpacity onPress={props.onPress}>
      <View style={containerStyles}>
        {isActive && <View style={AppStyle.navigation.drawer.item.leftBar} />}

        <Ionicons
          name={iconName}
          color={iconColor || "white"}
          size={24}
          containerStyle={AppStyle.navigation.drawer.item.icon}
        />
        <Text style={AppStyle.navigation.drawer.item.textLabel}>{label}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default DrawerItem;
