import React from "react";
import { View, Text } from "react-native";
import { Avatar } from "react-native-elements";
import AppStyle from "@styles/AppStyle";

const DrawerProfileContainer = (props: any) => {
  const { source, firstName, lastName, containerStyle } = props;
  const iconProps = source
    ? { source: source }
    : { icon: { name: "account-circle" } };

  return (
    <View
      style={[AppStyle.navigation.drawer.profile.container, containerStyle]}
    >
      <Avatar
        activeOpacity={0.7}
        rounded={true}
        containerStyle={AppStyle.navigation.drawer.profile.avatar}
        {...iconProps}
      />
      <Text style={AppStyle.navigation.drawer.profile.nameText}>
        {firstName} {lastName}
      </Text>
    </View>
  );
};

export default DrawerProfileContainer;
