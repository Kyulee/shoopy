import React from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import { Card, Button } from "react-native-elements";
import { useNavigation } from "@react-navigation/native";

import { addProduct, updateTotalQuantity } from "@store/slices/cartSlice";
import { useDispatch } from "react-redux";
import { Product } from "@services/models/product/product";
import { deleteProduct, getProducts } from "@store/slices/thunks/productThunks";
import AppStyle from "@styles/AppStyle";
import { unwrapResult } from "@reduxjs/toolkit";

interface IProductCardProps {
  product: Product;
  isAdmin?: boolean;
}

const ProductCard: React.FC<IProductCardProps> = ({
  product,
  isAdmin = false,
}) => {
  const navigation = useNavigation();
  const dispatch: any = useDispatch();
  return (
    <TouchableOpacity
      onPress={() =>
        !isAdmin && navigation.navigate("Product", { idProduct: product.id })
      }
    >
      <Card containerStyle={AppStyle.components.card.self}>
        <Image
          style={AppStyle.components.card.photo}
          source={{
            uri: product.photo,
          }}
        />
        <Text style={AppStyle.components.card.title}>{product.name}</Text>
        <Text style={AppStyle.components.card.price}>{product.price} €</Text>
        <View style={AppStyle.components.card.button}>
          {isAdmin ? (
            <>
              <Button
                title="Edit Product"
                type="outline"
                onPress={() =>
                  navigation.navigate("EditProduct", { idProduct: product.id })
                }
              />
              <Button
                title="Delete Product"
                type="outline"
                onPress={() =>
                  dispatch(deleteProduct({ idProduct: product.id })).then(() =>
                    dispatch(getProducts())
                  )
                }
              />
            </>
          ) : (
            <>
              <Button
                title="View details"
                type="outline"
                onPress={() =>
                  navigation.navigate("Product", { idProduct: product.id })
                }
              />
              <Button
                title="Add to cart"
                type="outline"
                onPress={() =>
                  dispatch(addProduct({ product })) &&
                  dispatch(updateTotalQuantity())
                }
              />
            </>
          )}
        </View>
      </Card>
    </TouchableOpacity>
  );
};

export default ProductCard;
