import React, { useState } from "react";
import { View, Text } from "react-native";
import { Card, Button } from "react-native-elements";
import { Order } from "@services/models/product/order";
import { formatDate } from "@services/utils/shared";
import AppStyle from "@styles/AppStyle";

interface IOrderCardProps {
  order: Order;
}

const OrderCard: React.FC<IOrderCardProps> = ({ order }: any) => {
  const [isShow, setIsShow] = useState(false);
  const orderItems = order.items;
  return (
    <View>
      <Card containerStyle={AppStyle.components.order.card}>
        <Text style={AppStyle.components.order.price}>Order n°{order.id}</Text>
        <Text style={AppStyle.components.order.price}>
          {formatDate(new Date(order.date))}
        </Text>
        <Text style={AppStyle.components.order.title}>
          ${order.totalOrder.toFixed(2)}
        </Text>
        <View style={AppStyle.components.order.button}>
          <Button
            title={isShow ? "Hide details" : "Show details"}
            type="outline"
            onPress={() => setIsShow(!isShow)}
          />
        </View>
        {isShow &&
          orderItems.map((item: any, index: any): any => (
            <View
              style={AppStyle.components.order.orderDetail}
              key={`item-${item.id}`}
            >
              <Text style={{ color: "darkgrey", fontWeight: "bold" }}>
                {item.qty}
              </Text>
              <Text style={{ fontWeight: "bold" }}>{item.name}</Text>
              <Text style={{ fontWeight: "bold" }}>${item.price}</Text>
            </View>
          ))}
      </Card>
    </View>
  );
};

export default OrderCard;
