export type Product = {
  id: string;
  name: string;
  photo: string;
  description: string;
  price: number;
};
