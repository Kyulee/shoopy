export type Order = {
  id: number;
  date: Date;
  totalOrder: number;
  items: Item[];
};

type Item = {
  qty: number;
  name: string;
  price: number;
};
