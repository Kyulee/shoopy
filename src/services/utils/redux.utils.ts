import { Alert } from "react-native";
import { AsyncStatus } from "./shared";

export const genericLoadingCase = (state: any) => {
  state.readStatus = AsyncStatus.LOADING;
};

export const genericErrorCase = (state: any) => {
  state.readStatus = AsyncStatus.ERROR;
  Alert.alert(
    "Erreur",
    "Problème est survenu lors du chargement des données veuillez reessayez"
  );
};

export const genericReadCase = (state: any, { payload }: any) => {
  state.readStatus = AsyncStatus.SUCCESS;
  state.content = Object.entries(payload).map(([key, value]) => value);
};
