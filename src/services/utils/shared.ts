import * as ImagePicker from "expo-image-picker";

const locale = "en-GB";
/* Ajoute le suffix th,st,nd,rd en fonction du jour dans le mois*/
const ordinalDate = (date: Date): string => {
  const day: number = date.getDate();
  const s = ["th", "st", "nd", "rd"];
  const v = day % 100;
  return `${day}${s[(v - 20) % 10] || s[v] || s[0]}`;
};

export const formatDate = (date: Date): string =>
  `${date.toLocaleDateString(locale, {
    month: "long",
  })} ${ordinalDate(date)} ${date.toLocaleDateString(locale, {
    year: "numeric",
    hour: "2-digit",
    minute: "2-digit",
  })}`;

export const pickImage = async () => {
  const permission = await ImagePicker.requestMediaLibraryPermissionsAsync();
  if (!permission) throw Error("Unauthorized picking image ! No voyeurisme");
  let result: any = await ImagePicker.launchImageLibraryAsync({
    mediaTypes: ImagePicker.MediaTypeOptions.All,
    allowsEditing: true,
    aspect: [4, 3],
    quality: 1,
    base64: true,
  });
  return `data:image/jpeg;base64,${result.base64}`;
};

export enum AsyncStatus {
  NONE,
  LOADING,
  SUCCESS,
  ERROR,
}
