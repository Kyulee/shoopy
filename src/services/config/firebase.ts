import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyAAJnr24R8bY4-O_sm59ZKQO-0VBZa6qnY",
  authDomain: "shoopy-app.firebaseapp.com",
  databaseURL:
    "https://shoopy-app-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "shoopy-app",
  storageBucket: "shoopy-app.appspot.com",
  messagingSenderId: "918321800203",
  appId: "1:918321800203:web:483c5e7af5f70b7335d30e",
};

let database;
export const initFirebase = () => {
  if (firebase.apps.length === 0) {
    firebase.initializeApp(firebaseConfig);
    database = firebase.database();
  }
  console.log("Firebase is on");
};
export const getDatabase = () => firebase.database();
