export const fakeProduct = [
  {
    id: "p0",
    name: "Baiocchi, biscuits fourrés à la crème aux noisettes et cacao",
    photo:
      "https://cdn.discordapp.com/attachments/823660396913229875/836961338097795112/Baiocchi.png",
    description: "Un petit frosty ? :p",
    price: 2.99,
  },
  {
    id: "p1",
    name: "Red Shirt",
    photo:
      "https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg",
    description: "A red t-shirt, perfect for days with non-red weather.",
    price: 29.99,
  },
  {
    id: "p2",
    name: "Blue Carpet",
    photo:
      "https://images.pexels.com/photos/6292/blue-pattern-texture-macro.jpg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
    description: "Fits your red shirt perfectly. To stand on. Not to wear it.",
    price: 99.99,
  },
  {
    id: "p3",
    name: "Coffee Mug",
    photo:
      "https://images.pexels.com/photos/160834/coffee-cup-and-saucer-black-coffee-loose-coffee-beans-160834.jpeg?cs=srgb&dl=bean-beans-black-coffee-160834.jpg&fm=jpg",
    description: "Can also be used for tea!",
    price: 8.99,
  },
  {
    id: "p4",
    name: "The Book - Limited Edition",
    photo:
      "https://images.pexels.com/photos/46274/pexels-photo-46274.jpeg?cs=srgb&dl=blur-blurred-book-pages-46274.jpg&fm=jpg",
    description:
      "What the content is? Why would that matter? It's a limited edition!",
    price: 15.99,
  },
  {
    id: "p5",
    name: "PowerBook",
    photo:
      "https://get.pxhere.com/photo/laptop-computer-macbook-mac-screen-water-board-keyboard-technology-air-mouse-photo-airport-aircraft-tablet-aviation-office-black-monitor-keys-graphic-hardware-image-pc-exhibition-multimedia-calculator-vector-water-cooling-floppy-disk-phased-out-desktop-computer-netbook-personal-computer-computer-monitor-electronic-device-computer-hardware-display-device-448748.jpg",
    description:
      "Awesome hardware, crappy keyboard and a hefty price. Buy now before a new one is released!",
    price: 2299.99,
  },
  {
    id: "p6",
    name: "Pen & Paper",
    photo: "https://cdn.pixabay.com/photo/2015/10/03/02/14/pen-969298_1280.jpg",
    description:
      "Can be used for role-playing (not the kind of role-playing you're thinking about...).",
    price: 5.49,
  },
];
export const fakeOrder = [
  {
    id: 1,
    date: new Date().getTime(),
    totalOrder: 21,
    items: [
      {
        qty: 2,
        name: "Red T-Shirt",
        price: 59.95,
      },
      {
        qty: 20,
        name: "Baiocchi",
        price: 5.95,
      },
    ],
  },
  {
    id: 2,
    date: new Date().getTime(),
    totalOrder: 211.23,
    items: [
      {
        qty: 2,
        name: "Red T-Shirt",
        price: 59.95,
      },
      {
        qty: 2,
        name: "Red T-Shirt",
        price: 59.95,
      },
    ],
  },
];

export const getProductById = (id: string) =>
  fakeProduct.find((product) => product.id === id);

export default { fakeProduct, fakeOrder };
